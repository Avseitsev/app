// @flow

import React from "react"
import Area from "./components/Area"
import Toolbar from "./components/Toolbar"
// $FlowFixMe
import "./App.scss"

export default function App(): React$Node {
  return (
    <div className="body">
      <Toolbar />
      <Area />
    </div>
  )
}
