// @flow

import React, { Fragment } from "react"
import { connect } from 'react-redux'
import ResetAction from "./components/ResetAction"
import TwoPointsAction from "./components/TwoPointsAction"
import FillAction from "./components/FillAction"
import History from "./components/History"
import SaveButton from "./components/SaveButton"
// $FlowFixMe
import "./Toolbar.scss"

import type { Matrix } from "../../types/areaTypes"

type PropsFromState = {
  matrix: Matrix,
  error: string,
}

function Toolbar({ matrix, error }: PropsFromState): React$Node {
  return (
    <div className="toolbar">
      {error && (
        <div className="toolbar__error">{error}</div>
      )}
      <ResetAction />
      {matrix && (
        <Fragment>
          <TwoPointsAction title="линия" actionType="LINE" />
          <TwoPointsAction title="прямоуг" actionType="RECTANGLE" />
          <FillAction />
          <SaveButton />
          <History />
        </Fragment>
      )}
    </div>
  )
}

const mapStateToProps = ({ matrix, error }): PropsFromState => {
  return {
    matrix,
    error,
  }
}
// $FlowFixMe
export default connect(mapStateToProps)(Toolbar)
