// @flow

import React, { PureComponent } from "react"
import { connect } from "react-redux"
import { valueToNumber } from "../utils"

type PropsFromDispatch = {
  onReset: (w: number, h: number) => void,
}

type State = {
  w: string,
  h: string,
}

export class ResetAction extends PureComponent <PropsFromDispatch, State> {
  state: State = {
    w: "",
    h: "",
  }

  onReset = (): void => {
    const { w, h } = this.state
    const { onReset } = this.props
    if (!w || !h) return
    onReset(Number(w), Number(h))
  }

  onChangeValue = (e, key: string): void => {
    this.setState({
      [key]: valueToNumber(e.target.value)
    })
  }

  render() {
    const { w, h } = this.state
    return (
      <div className="toolbar__action">
        <span>ресет:</span>
        <input placeholder="ширина"  value={w} onChange={(e) => this.onChangeValue(e, "w")}/>
        <input placeholder="высота" value={h} onChange={(e) => this.onChangeValue(e, "h")}/>
        <button disabled={!w || !h} onClick={this.onReset}>OK</button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch): PropsFromDispatch => {
  return {
    onReset: (w: number, h: number) => dispatch({ type: 'RESET', payload: { w, h } }),
  }
}
// $FlowFixMe
export default connect(null, mapDispatchToProps)(ResetAction)
