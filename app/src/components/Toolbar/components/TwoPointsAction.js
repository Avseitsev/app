// @flow

import React, { PureComponent } from "react"
import { connect } from "react-redux"
import { valueToNumber } from "../utils"

type PropsFromDispatch = {
  onAction: (x: number, y: number, x2: number, y2: number) => void,
}

type Props = {
  title: string,
  actionType: string,
} & PropsFromDispatch

type State = {
  x: string,
  y: string,
  x2: string,
  y2: string,
}

class TwoPointsAction extends PureComponent <Props, State> {
  state: State = {
    x: "",
    y: "",
    x2: "",
    y2: "",
  }

  onAction = (): void => {
    const { x, y, x2, y2 } = this.state
    const { onAction } = this.props
    if (!x || !y || !x2 || !y2) return
    onAction(Number(x), Number(y), Number(x2), Number(y2))
  }

  onChangeValue = (e, key: string): void => {
    this.setState({
      [key]: valueToNumber(e.target.value)
    })
  }

  render() {
    const { x, y, x2, y2 } = this.state
    const { title } = this.props
    return (
      <div className="toolbar__action">
        <span>{title}:</span>
        <input placeholder="x"  value={x} onChange={(e) => this.onChangeValue(e, "x")}/>
        <input placeholder="y" value={y} onChange={(e) => this.onChangeValue(e, "y")}/>
        <input placeholder="x2"  value={x2} onChange={(e) => this.onChangeValue(e, "x2")}/>
        <input placeholder="y2" value={y2} onChange={(e) => this.onChangeValue(e, "y2")}/>
        <button disabled={!x || !y || !x2 || !y2} onClick={this.onAction}>OK</button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props): PropsFromDispatch => {
  return {
    onAction: (x: number, y: number, x2: number, y2: number) => dispatch({ type: props.actionType , payload: { x, y, x2, y2 } }),
  }
}
// $FlowFixMe
export default connect(null, mapDispatchToProps)(TwoPointsAction)

