import React from "react"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"
import renderer from "react-test-renderer"
import { ResetAction } from "../ResetAction"

configure({adapter: new Adapter()})

describe("ResetAction", () => {
  test("Snapshot", ()=> {
    const component = renderer
      .create(<ResetAction onReset={jest.fn()} />)
      .toJSON()
    expect(component).toMatchSnapshot()
  })
  
  test("init state", ()=> {
    const component = shallow(<ResetAction onReset={jest.fn()} />)
    expect(component.instance().state).toEqual({
      w: "",
      h: "",
    })
  })
  
  test("onReset should return", ()=> {
    const onReset = jest.fn()
    const component = shallow(<ResetAction onReset={onReset} />)
    expect(onReset).not.toBeCalled()
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
    component.instance().setState({
      w: "5",
      h: "",
    })
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
    component.instance().setState({
      w: "",
      h: "5",
    })
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
  })
  
  test("onReset should return", ()=> {
    const onReset = jest.fn()
    const component = shallow(<ResetAction onReset={onReset} />)
    expect(onReset).not.toBeCalled()
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
    component.instance().setState({
      w: "5",
      h: "",
    })
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
    component.instance().setState({
      w: "",
      h: "5",
    })
    component.instance().onReset()
    expect(onReset).not.toBeCalled()
  })

  test("onReset should call props.onReset", ()=> {
    const onReset = jest.fn()
    const component = shallow(<ResetAction onReset={onReset} />)
    expect(onReset).not.toBeCalled()
    component.instance().setState({
      w: "5",
      h: "4",
    })
    component.instance().onReset()
    expect(onReset).toBeCalledWith(5, 4)
  })

  test("onChangeValue", ()=> {
    const component = shallow(<ResetAction onReset={jest.fn()} />)
    expect(component.instance().state).toEqual({
      w: "",
      h: "",
    })
    component.instance().onChangeValue({ target: { value: "6" } }, "w")
    expect(component.instance().state).toEqual({
      w: "6",
      h: "",
    })
  })
})
