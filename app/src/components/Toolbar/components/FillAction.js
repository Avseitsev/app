// @flox

import React, { PureComponent } from "react"
import { connect } from "react-redux"
import { valueToNumber } from "../utils"

type PropsFromDispatch = {
  onReset: (x: number, h: number, color: symbol) => void,
}

type State = {
  x: string,
  y: string,
  color: string,
}

class FillAction extends PureComponent <PropsFromDispatch, State> {
  state: State = {
    x: "",
    y: "",
    color: "",
  }

  onReset = (): void => {
    const { x, y, color } = this.state
    const { onReset } = this.props
    if (!x || !y || !color) return
    onReset(Number(x), Number(y), color)
  }

  onChangeValue = (e, key: string): void => {
    this.setState({
      [key]: valueToNumber(e.target.value)
    })
  }

  onChangeColor = (e): void => {
    this.setState({
      color:  (e.target.value[0] || "").toUpperCase(),
    })
  }

  render() {
    const { x, y, color } = this.state
    return (
      <div className="toolbar__action">
        <span>заливка:</span>
        <input placeholder="x"  value={x} onChange={(e) => this.onChangeValue(e, "x")}/>
        <input placeholder="y" value={y} onChange={(e) => this.onChangeValue(e, "y")}/>
        <input placeholder="цвет" value={color} onChange={this.onChangeColor}/>
        <button disabled={!x || !y || !color} onClick={this.onReset}>OK</button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch): PropsFromDispatch => {
  return {
    onReset: (x: number, y: number, color: symbol) => dispatch({ type: 'FILL', payload: { x, y, color } }),
  }
}
// $FloxFixMe
export default connect(null, mapDispatchToProps)(FillAction)
