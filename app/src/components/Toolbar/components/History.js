// @flow

import React from "react"
import { connect } from 'react-redux'

type PropsFromState = {
  history: Array<string>,
}

function History({ history }: PropsFromState): React$Node {
  return (
    <div className="toolbar__history">
    <div>ИСТОРИЯ</div>
    <div>
      {history.map(text => (
        <div key={text}>{text}</div>
      ))}
    </div>
    </div>
  )
}

const mapStateToProps = ({ history }): PropsFromState => {
  return {
    history,
  }
}
// $FlowFixMe
export default connect(mapStateToProps)(History)