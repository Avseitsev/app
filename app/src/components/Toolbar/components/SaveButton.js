// @flow

import React, { PureComponent } from "react"
import { connect } from 'react-redux'

import type { Matrix } from "../../../types/areaTypes"

type PropsFromState = {
  history: Array<string>,
  matrix: Matrix,
}

class SaveButton extends PureComponent <PropsFromState, {}> {
  onClick = (): void => {
    const { history, matrix } = this.props
    const body = document.body
    if (!body || !matrix) return
    const matrixToWrite = matrix.reduce((accumulator, line) => {
      const newLine = line.reduce((accumulator, currentValue) => {
        return accumulator + (currentValue || " ")
      }, "")
      return  accumulator + "|" + newLine + "|\n"
    }, "")
    const line = new Array(matrix[0].length + 3).join("-") + "\n"
    const historyToWrite = history.reduce((accumulator, line) => {
      return accumulator + line + "\n"
    }, "")
    const valueToWrite = line + matrixToWrite + line + "\n" + "ИСТОРИЯ" + "\n" + historyToWrite
    const download = document.createElement("a");
    download.href = "data:text/plain;content-disposition=attachment;filename=file," + valueToWrite
    download.download = "output.txt"
    download.style.display = "none"
    download.id = "download"
    body.appendChild(download)
    const element = document.getElementById("download")
    if (element) {
      element.click()
    }
    body.removeChild(download)
  }

  render() {
    return (
      <button className="toolbar__save-button" onClick={this.onClick}>
        скачать результат
      </button>
    )
  }
}

const mapStateToProps = ({ history, matrix }): PropsFromState => {
  return {
    history,
    matrix,
  }
}
// $FlowFixMe
export default connect(mapStateToProps)(SaveButton)