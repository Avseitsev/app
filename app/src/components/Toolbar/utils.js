// @flow

export const valueToNumber = (value: string): string => value.replace(/\D/g,'')
