// @flow

import React from "react"
import shortid from "shortid"

type Props = {
  line: Array<?string>,
}

export default function Line({ line }: Props): React$Node {
  return (
    <div className="area__line">
      {line.map((elem) => <div className="area__elem" key={shortid.generate()}>{elem}</div>)}
    </div>
  )
}
