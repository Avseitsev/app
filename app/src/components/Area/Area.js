// @flow

import React from "react"
import { connect } from "react-redux"
import shortid from "shortid"
import Line from "./components/Line"
// $FlowFixMe
import "./Area.scss"

import type { Matrix } from "../../types/areaTypes"

type PropsFromState = {
  matrix: Matrix,
}

function Area({ matrix }: PropsFromState): React$Node {
  if (!matrix) return null
  return (
    <div className="area">
      {matrix.map((line) => <Line line={line} key={shortid.generate()} />)}
    </div>
  )
}

const mapStateToProps = ({ matrix }): PropsFromState => {
  return {
    matrix,
  }
}
// $FlowFixMe
export default connect(mapStateToProps)(Area)
