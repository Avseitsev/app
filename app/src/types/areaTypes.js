// @flow

export type Matrix = ?Array<Array<?string>>
