// @flow

import { pointInArea, numberInRange, copyMatrix } from "../utils/areaUtils"

import type { Matrix } from "../types/areaTypes"

type State = {
  matrix: Matrix,
  error: ?string,
  history: Array<string>,
}

const defaultState: State = {
  matrix: null,
  error: null,
  history: [],
}

export default (state: State = defaultState, action: { type: string, payload: Object }): State => {
  switch (action.type) {
    case "RESET": {
      const { w, h } = action.payload
      if (w <= 0 || h <= 0) {
        return { ...state, error: "нельзя создать область меньше нуля" }
      }
      if (w > 100 || h > 100) {
        return { ...state, error: "не ну больше 100 нельзя - память не бесконечная да и рендарит долго" }
      }
      const voidMatrix = Array(h).fill().map(() => Array(w).fill())
      return { ...defaultState, history: [`res ${w} ${h}`], matrix: voidMatrix }
    }
    case "LINE": {
      const { x, y, x2, y2 } = action.payload
      const { matrix } = state
      const newMatrix = copyMatrix(matrix)
      if (!matrix || !newMatrix) {
        return { ...state, error: "матрица пустая" }
      }
      const isVertical = y !== y2
      if (x !== x2 && isVertical) {
        return { ...state, error: "по диагонали нельзя!" }
      }
      if (!pointInArea(matrix, x, y) || !pointInArea(matrix, x2, y2)) {
        return { ...state, error: "выхгодит за рамки" }
      }
      const h = newMatrix.length
      if (isVertical) {
        newMatrix.forEach((item, index) => {
          const increasedIndex = index + 1
          if (numberInRange(increasedIndex, y, y2)) {
            newMatrix[h - increasedIndex][x - 1] = "X"
          }
        })
      } else {
        const toRight = x2 > x
        newMatrix[h - y].fill("X", toRight ? x - 1 : x2 - 1 , toRight ? x2 : x)
      }
      return { ...state, error: null, history: [...state.history, `l ${x} ${y} ${x2} ${y2}`], matrix: newMatrix }
    }
    case "RECTANGLE": {
      const { x, y, x2, y2 } = action.payload
      const { matrix } = state
      const newMatrix = copyMatrix(matrix)
      if (!matrix || !newMatrix) {
        return { ...state, error: "матрица пустая" }
      }
      if (!pointInArea(matrix, x, y) || !pointInArea(matrix, x2, y2)) {
        return { ...state, error: "выхгодит за рамки" }
      }
      const h = newMatrix.length
      const toRight = x2 > x
      newMatrix[h - y].fill("X", toRight ? x - 1 : x2 - 1 , toRight ? x2 : x)
      newMatrix[h - y2].fill("X", toRight ? x - 1 : x2 - 1 , toRight ? x2 : x)
      newMatrix.forEach((item, index) => {
        const increasedIndex = index + 1
        if (numberInRange(increasedIndex, y, y2)) {
          newMatrix[h - increasedIndex][x - 1] = "X"
          newMatrix[h - increasedIndex][x2 - 1] = "X"
        }
      })
      return { ...state, error: null, history: [...state.history, `rec ${x} ${y} ${x2} ${y2}`], matrix: newMatrix }
    }
    case "FILL": {
      const { matrix } = state
      const { x, y, color } = action.payload
      const newMatrix = copyMatrix(matrix)
      if (!matrix || !newMatrix) {
        return { ...state, error: "матрица пустая" }
      }
      if (!pointInArea(matrix, x, y)) {
        return { ...state, error: "выхгодит за рамки" }
      }
      if (color.length > 1) {
        return { ...state, error: "цвет - 1 символ" }
      }
      const h = newMatrix.length
      const currentSymbol = newMatrix[h - y][x - 1]
      if (currentSymbol === color) {
        return { ...state, error: "уже покрашено в этот цвет!" }
      }
      const mark = "##"
      newMatrix[h - y][x - 1] = mark
      let markedCount = 1
      while (markedCount) {
        markedCount = 0
        newMatrix.forEach((line, y) => {
          line.forEach((item, x) => {
            if (item === mark) {
              markedCount++
              newMatrix[y][x] = color
              if (pointInArea(matrix, x + 2, y + 1) && newMatrix[y][x + 1] === currentSymbol) {
                newMatrix[y][x + 1] = mark
              }
              if (pointInArea(matrix, x, y + 1) && newMatrix[y][x - 1] === currentSymbol) {
                newMatrix[y][x - 1] = mark
              }
              if (pointInArea(matrix, x + 1, y + 2) && newMatrix[y + 1][x] === currentSymbol) {
                newMatrix[y + 1][x] = mark
              }
              if (pointInArea(matrix, x + 1, y) && newMatrix[y - 1][x] === currentSymbol) {
                newMatrix[y - 1][x] = mark
              }
            }
          })
        })
      }
      return { ...state, error: null, history: [...state.history, `f ${x} ${y} ${color}`], matrix: newMatrix }
    }
    default:
      return state
  }
}
