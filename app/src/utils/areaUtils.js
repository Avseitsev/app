// @flow

import type { Matrix } from "../types/areaTypes"

export const pointInArea = (area: Matrix, x: number, y: number): boolean => {
  if (!area) return false
  if (x < 1 || y < 1) return false
  if (y > area.length || x > area[0].length) return false
  return true
}

export const numberInRange = (value: number, from: number, to: number): boolean => {
  if ((value >= from && value <= to) || (value <= from && value >= to)) {
    return true
  }
  return false
}

export const copyMatrix = (matrix: Matrix): Matrix => { // нужэно что бы не было мутаций в матрице, обычный слайс тут не поможет
  if (!matrix) return []
  return matrix.reduce((accumulator, currentValue) => {
    return [...accumulator, currentValue.slice()]
  }, [])
}
